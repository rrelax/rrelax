# Hey!

Welcome to my GitLab profile! This is my work account, please see my [personal account](https://gitlab.com/mcatee) or [website](https://mcatee.io) for some of my projects.

Feedback (whether personal or project related) is always welcome! Feel free to reach out on any channel listed on my site, otherwise you can create an issue [here (but please ensure it's marked as confidential!)](https://gitlab.com/rrelax/rrelax/-/issues/new?issue[confidential]=true&issue[title]=Feedback%20-%20(topic))
